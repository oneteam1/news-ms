package com.newstok.news;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
class NewsMsApplicationTests {

	@Autowired
	WebApplicationContext webApplicationContext;

	@Test
	void testGetNewsById() throws Exception {
		MvcResult result = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
				.perform(MockMvcRequestBuilders.get("/newstok/news/N1001")).andReturn();
		assertEquals(200, result.getResponse().getStatus());
		assertTrue(result.getResponse().getContentAsString().contains("Jio"));
	}

}
