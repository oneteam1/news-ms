/**
 * 
 */
package com.newstok.news.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newstok.common.model.News;
import com.newstok.news.service.NewsService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author PUSHPENDRA
 *
 */
@RestController
@RequestMapping(value = "/newstok")
@Slf4j
@CrossOrigin
public class NewsController {

	@Autowired
	private NewsService newsService;

	@GetMapping(value = "/news", produces = { "application/json" })
	public ResponseEntity<List<News>> getNews() {
		log.info("Getting news");
		List<News> newsList = newsService.getNews();
		return ResponseEntity.status(HttpStatus.OK).body(newsList);
	}

	@GetMapping(value = "/news/{id}", produces = { "application/json" })
	public ResponseEntity<News> getNewsById(@PathVariable String id) {
		log.info("Getting news details for {}", id);
		News news = newsService.getNewsById(id);
		return ResponseEntity.status(HttpStatus.OK).body(news);
	}

}
