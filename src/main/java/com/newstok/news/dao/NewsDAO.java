package com.newstok.news.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "news")
public class NewsDAO {

	@DynamoDBHashKey(attributeName = "news_id")
	private String id;

	@DynamoDBAttribute(attributeName = "type")
	private String type;

	@DynamoDBAttribute(attributeName = "title")
	private String title;

	@DynamoDBAttribute(attributeName = "subtitle")
	private String subtitle;

	@DynamoDBAttribute(attributeName = "description")
	private String description;

	@DynamoDBAttribute(attributeName = "has-media")
	private boolean hasMedia;

	@DynamoDBAttribute(attributeName = "media_link")
	private String mediaLink;

	@DynamoDBAttribute(attributeName = "area-code")
	private String areaCode;

	@DynamoDBAttribute(attributeName = "city")
	private String city;

	@DynamoDBAttribute(attributeName = "created")
	private String created;

	@DynamoDBAttribute(attributeName = "user-id")
	private String userId;
}
