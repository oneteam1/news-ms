package com.newstok.news.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.newstok.common.model.News;
import com.newstok.news.dao.NewsDAO;
import com.newstok.news.mapper.DataMapper;

@Service
public class NewsServiceImpl implements NewsService {

	@Autowired
	private DynamoDBMapper dyMapper;

	@Autowired
	private DataMapper dataMapper;

	@Override
	public News getNewsById(String newsId) {
		NewsDAO newsDao = dyMapper.load(NewsDAO.class, newsId);
		return dataMapper.daoToObj(newsDao);
	}

	@Override
	public List<News> getNews() {
		PaginatedScanList<NewsDAO> paginatedList = dyMapper.scan(NewsDAO.class, new DynamoDBScanExpression());
		List<News> newsList = new ArrayList<>();
		for (NewsDAO newsDAO : paginatedList) {
			newsList.add(dataMapper.daoToObj(newsDAO));
		}
		return newsList;
	}

}
