package com.newstok.news.service;

import java.util.List;

import com.newstok.common.model.News;

public interface NewsService {

	News getNewsById(String newsId);
	List<News> getNews();
}
