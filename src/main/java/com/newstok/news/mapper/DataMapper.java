package com.newstok.news.mapper;

import com.newstok.common.model.News;
import com.newstok.news.dao.NewsDAO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DataMapper {

	News daoToObj(NewsDAO newsDao);

}
