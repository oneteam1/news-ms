package com.newstok.news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.newstok" })
public class NewsMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsMsApplication.class, args);
	}

}
