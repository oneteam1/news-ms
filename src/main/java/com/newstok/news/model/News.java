/*
 * package com.newstok.news.model;
 * 
 * import org.springframework.context.annotation.Scope; import
 * org.springframework.stereotype.Component;
 * 
 * import lombok.Data;
 * 
 * @Component
 * 
 * @Scope("prototype")
 * 
 * @Data public class News { private String id; private String type; private
 * String heading; private String subHeading; private String resourceLink; }
 */